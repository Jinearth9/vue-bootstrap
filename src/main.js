import Vue from 'vue'
import VueRouter from 'vue-router'
import BootstrapVue from 'bootstrap-vue'
import Vuelidate from 'vuelidate'
import './assets/fonts/stroke-7/style.css'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import './assets/sass/app.scss'
import App from './App.vue'
import { routes } from './routes'
import store from './store/store'

Vue.config.productionTip = false;

Vue.use(VueRouter);
Vue.use(BootstrapVue);
Vue.use(Vuelidate);

const router = new VueRouter({
    mode: 'history',
    routes
});

new Vue({
    render: h => h(App),
    router,
    store
}).$mount('#app');
