import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        forms: {},
        profiles: [],
        id: 1
    },
    getters: {
        forms: state => {
            return state.forms
        },
        profiles: state => {
            return state.profiles
        }
    },
    mutations: {
        'SAVE_INFORMATIONS': (state, infos) => {
            if (infos.selectClassified == 'Yes') {
                Object.assign(state.forms, {
                    uploadedFile: infos.uploadedFile,
                    selectAreaOfConcern: infos.selectAreaOfConcern,
                    selectHowLong: infos.selectHowLong,
                    selectShapeOrColor: infos.selectShapeOrColor,
                    selectClassified: infos.selectClassified,
                    selectClassification: infos.selectClassification,
                    selectWhenClassified: infos.selectWhenClassified,
                    selectClassifiedBy: infos.selectClassifiedBy,
                    selectCurrentlyInTreatment: infos.selectCurrentlyInTreatment,
                    selectPastTreatment: infos.selectPastTreatment
                })
            } else {
                Object.assign(state.forms, {
                    uploadedFile: infos.uploadedFile,
                    selectAreaOfConcern: infos.selectAreaOfConcern,
                    selectHowLong: infos.selectHowLong,
                    selectShapeOrColor: infos.selectShapeOrColor,
                    selectClassified: infos.selectClassified
                })
            }
        },
        'SET_PROFILES': (state) => {
            const date = new Date();
            const dateFormatted = date.toLocaleDateString();
            state.profiles.push({
                id: state.id++,
                image: state.forms.uploadedFile,
                area: state.forms.selectAreaOfConcern,
                classification: '',
                date: dateFormatted,
                actions: {
                    edit: 'uploads/upload',
                    details: '/results/result'
                }
            })
        }
    },
    actions: {
        saveInformations: ({commit}, infos) => {
            commit('SAVE_INFORMATIONS', infos)
        },
        setProfiles: ({commit}) => {
            commit('SET_PROFILES')
        }
    }
})