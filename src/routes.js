const Home = () => import('./components/Home.vue');
const Login = () => import('./components/Login.vue');
const Signup = () => import('./components/Signup.vue');
const Welcome = () => import('./components/info/Welcome.vue');
const Background = () => import('./components/info/Background.vue');
const MedicalHistory = () => import('./components/info/MedicalHistory.vue');
const Guideline = () => import('./components/upload/Guideline.vue');
const Upload = () => import('./components/upload/Upload.vue');
const Preview = () => import('./components/upload/Preview.vue');
const About = () => import('./components/result/About.vue');
const Results = () => import('./components/result/Results.vue');
const Profile = () => import('./components/Profile.vue');

export const routes = [
    { path: '/', name: 'home', component: Home },
    { path: '/login', name: 'login', component: Login },
    { path: '/signup', name: 'signup', component: Signup },
    { path: '/info', name: 'info', component: Welcome },
    { path: '/info/background', name: 'background', component: Background },
    { path: '/info/history', name: 'history', component: MedicalHistory },
    { path: '/uploads', name: 'uploads', component: Guideline },
    { path: '/uploads/upload', name: 'upload', component: Upload },
    { path: '/upload/preview', name: 'preview', component: Preview },
    { path: '/results', name: 'results', component: About },
    { path: '/results/result', name: 'result', component: Results },
    { path: '/profile', name: 'profile', component: Profile }
];